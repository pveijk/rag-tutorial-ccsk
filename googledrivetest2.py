#https://github.com/pprados/langchain-googledrive/blob/master/docs/integrations/document_loaders/google_drive.ipynb
# see authentication details. Need to register an application.

from langchain_googledrive.document_loaders import GoogleDriveLoader
#folder_id = "root"
folder_id='101Qy90vW7_6WRc2M0NYW3Llrh3vVAbSd'
#folder_id='1Wh0DxvR-kpkRWGiebhnYLFb9eAXIuqfy'
# https://drive.google.com/drive/folders/101Qy90vW7_6WRc2M0NYW3Llrh3vVAbSd shared folder

import logging

logging.basicConfig(level=logging.INFO)

loader = GoogleDriveLoader(
    folder_id=folder_id,
    recursive=False,
    num_results=3,  # Maximum number of file to load
    file_type=["application/vnd.google-apps.document"],
    includeItemsFromAllDrives=True
)

docs = loader.load()

doc=docs[1]
# Ensure the filename is valid and handle special characters
def sanitize_filename(filename):
    return "".join(char if char.isalnum() or char in " ._-" else "_" for char in filename)

# Retrieve the title from the document's metadata and sanitize it for the filename
filename = sanitize_filename(f"{doc.metadata['title']}.txt")

# Write the page content to the file
with open(filename, "w", encoding="utf-8") as file:
    file.write(doc.page_content)

# Confirm that the document was saved
print(f"Saved document '{doc.metadata['title']}' to '{filename}'")

for doc in docs:
    print("---")
    print(doc.metadata)
    print(doc.page_content.strip()[:300] + "...")
# print(len(doc))