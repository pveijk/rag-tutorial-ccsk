# https://python.langchain.com/v0.2/docs/how_to/document_loader_pdf/#using-unstructured
from langchain_community.document_loaders import PyPDFLoader

file_path = (
    "content/Domain 1 (FINAL)_ Cloud Computing Concepts & Architectures.pdf"
)
loader = PyPDFLoader(file_path)
pages = loader.load_and_split()

print(pages[0].page_content[:300])
print(pages[0].metadata)

from langchain_community.document_loaders import UnstructuredPDFLoader

file_path = (
    "content/Domain 1 (FINAL)_ Cloud Computing Concepts & Architectures.pdf"
)
loader = UnstructuredPDFLoader(file_path)
data = loader.load()

print(data[0].page_content[:300])
print(data[0].metadata)