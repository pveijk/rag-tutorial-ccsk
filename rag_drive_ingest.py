# -*- coding: utf-8 -*-
"""Rag Google Drive ingest

Adapted from https://python.langchain.com/v0.2/docs/tutorials/rag/

https://python.langchain.com/v0.2/docs/how_to/document_loader_pdf/
"""

from dotenv import load_dotenv
import os

load_dotenv()

os.environ["LANGCHAIN_TRACING_V2"] = "true"
#os.environ["LANGCHAIN_API_KEY"] = getpass.getpass()
# https://smith.langchain.com/ p@d1g.nl

#os.environ["OPENAI_API_KEY"] = getpass.getpass()
# user name peter@digitalinfrastructures.nl

from langchain_openai import ChatOpenAI

llm = ChatOpenAI(model="gpt-3.5-turbo-0125")

from langchain_chroma import Chroma
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_openai import OpenAIEmbeddings
from langchain_text_splitters import RecursiveCharacterTextSplitter

#https://github.com/pprados/langchain-googledrive/blob/master/docs/integrations/document_loaders/google_drive.ipynb
# see authentication details. Need to register an application.

from langchain_googledrive.document_loaders import GoogleDriveLoader
#folder_id = "root"
folder_id='101Qy90vW7_6WRc2M0NYW3Llrh3vVAbSd'
#folder_id='1Wh0DxvR-kpkRWGiebhnYLFb9eAXIuqfy'
# https://drive.google.com/drive/folders/101Qy90vW7_6WRc2M0NYW3Llrh3vVAbSd shared folder

import logging

loader = GoogleDriveLoader(
    folder_id=folder_id,
    recursive=False,
    num_results=20,  # Maximum number of file to load
    file_type=["application/vnd.google-apps.document"],
    suggestionsViewMode="PREVIEW_SUGGESTIONS_ACCEPTED",
    includeItemsFromAllDrives=True
)

docs = loader.load()

for doc in docs:
    print("---")
    print(doc.metadata)
#    print(doc.page_content.strip()[:300] + "...")

# splitting
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain_text_splitters import MarkdownHeaderTextSplitter
# https://python.langchain.com/v0.2/docs/how_to/markdown_header_metadata_splitter/
# statistical splitter? How to know it is any good?

headers_to_split_on = [
    ("#", "Header 1"),
    ("##", "Header 2"),
    ("###", "Header 3"),
    ("####", "Header 4"),
]

# text_splitter = RecursiveCharacterTextSplitter(
#     chunk_size=1000, chunk_overlap=200, add_start_index=True
# )
# all_splits = text_splitter.split_documents(docs)

markdown_splitter = MarkdownHeaderTextSplitter(headers_to_split_on, strip_headers=False)
md_header_splits = []
for doc in docs:
    md_header_splits.extend(markdown_splitter.split_text(doc.page_content))

#storing and retrieving
from langchain_chroma import Chroma
from langchain_openai import OpenAIEmbeddings
all_splits = md_header_splits

# Re-initialize the vector store
import shutil
# This is the directory where the Chroma vectorstore is persisted
persist_directory = "./chroma_db"

# Delete the directory and all of its contents
shutil.rmtree(persist_directory)

vectorstore = Chroma.from_documents(documents=all_splits, embedding=OpenAIEmbeddings(), persist_directory="./chroma_db")
#vectorstore = Chroma.from_documents(embedding=OpenAIEmbeddings(), persist_directory=persist_directory)
retriever = vectorstore.as_retriever(search_type="similarity", search_kwargs={"k": 6})

#chain3. customizing the prompt
from langchain_core.prompts import PromptTemplate

def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

template = """Use the following pieces of context to answer the question at the end.
Only use information you can find in the context below.
If you don't know the answer, just say that you don't know, don't try to make up an answer.
If the provided context contradict, say so. 
Include the header under which you find the answer.
Use three sentences maximum and keep the answer as concise as possible.
Always say "thanks for asking!" at the end of the answer.

{context}

Question: {question}

Helpful Answer:"""
custom_rag_prompt = PromptTemplate.from_template(template)

rag_chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | custom_rag_prompt
    | llm
    | StrOutputParser()
)
print("\nfinal answer")

response = rag_chain.invoke("What is IaaS?")
print(response)