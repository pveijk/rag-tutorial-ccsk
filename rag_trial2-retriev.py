
from dotenv import load_dotenv

load_dotenv()

#os.environ["LANGCHAIN_TRACING_V2"] = "true"
#os.environ["LANGCHAIN_API_KEY"] = getpass.getpass()
# https://smith.langchain.com/ p@d1g.nl

#os.environ["OPENAI_API_KEY"] = getpass.getpass()
# user name peter@digitalinfrastructures.nl

from langchain_openai import ChatOpenAI

llm = ChatOpenAI(model="gpt-3.5-turbo-0125")

import bs4
from langchain import hub
from langchain_chroma import Chroma
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_openai import OpenAIEmbeddings
from langchain_text_splitters import RecursiveCharacterTextSplitter

from langchain_chroma import Chroma
from langchain_openai import OpenAIEmbeddings

#vectorstore = Chroma.from_documents(documents=all_splits, embedding=OpenAIEmbeddings(), persist_directory="./chroma_db")
vectorstore = Chroma(embedding_function=OpenAIEmbeddings(), persist_directory="./chroma_db")
retriever = vectorstore.as_retriever(search_type="similarity", search_kwargs={"k": 6})

# retrieved_docs = retriever.invoke("What is IaaS?")

# len(retrieved_docs)

# print(retrieved_docs[0].page_content)
#chain3. customizing the prompt
from langchain_core.prompts import PromptTemplate

def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

template = """Use the following pieces of context to answer the question at the end.
Only use information you can find in the context below.
If you don't know the answer, just say that you don't know, don't try to make up an answer.
If the provided context contradict, say so. 
Include the header, as indicated by a number of hashmarks, under which you find the answer.
Use three sentences maximum and keep the answer as concise as possible.
Always say "thanks for asking!" at the end of the answer.

{context}

Question: {question}

Helpful Answer:"""
custom_rag_prompt = PromptTemplate.from_template(template)

rag_chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | custom_rag_prompt
    | llm
    | StrOutputParser()
)
print("\nfinal answer")
response = rag_chain.invoke("What is the last name of Elvis?")
print(response)
# response = rag_chain.invoke("What is IaaS?")
# print(response)
# response = rag_chain.invoke("What is different than access controls?")
# print(response)

response = rag_chain.invoke("What headers and subheaders are there in this text?")
print(response)