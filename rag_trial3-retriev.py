# objective: to create a terminal/cli chatbot, no history yet
# (inspiration) https://python.langchain.com/v0.1/docs/use_cases/question_answering/sources/
from dotenv import load_dotenv

load_dotenv()

#os.environ["LANGCHAIN_TRACING_V2"] = "true"
#os.environ["LANGCHAIN_API_KEY"] = getpass.getpass()
# https://smith.langchain.com/ p@d1g.nl

#os.environ["OPENAI_API_KEY"] = getpass.getpass()
# user name peter@digitalinfrastructures.nl

from langchain_openai import ChatOpenAI

llm = ChatOpenAI(model="gpt-3.5-turbo-0125")

import bs4
from langchain import hub
from langchain_chroma import Chroma
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_openai import OpenAIEmbeddings
from langchain_text_splitters import RecursiveCharacterTextSplitter

from langchain_chroma import Chroma
from langchain_openai import OpenAIEmbeddings

vectorstore = Chroma(embedding_function=OpenAIEmbeddings(), persist_directory="./chroma_db")
retriever = vectorstore.as_retriever(search_type="similarity", search_kwargs={"k": 6})

from langchain_core.prompts import PromptTemplate

def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

template1 = """Use the following pieces of context to answer the question at the end.
Only use information you can find in the context below.
If you don't know the answer, just say that you don't know, don't try to make up an answer.
If the provided context contradict, say so. 
Include the header, as indicated by a number of hashmarks, under which you find the answer.
Use three sentences maximum and keep the answer as concise as possible.
Always say "thanks for asking!" at the end of the answer.

{context}

Question: {question}

Helpful Answer:"""

template2 = """For each of the following pieces of context, analyse to what extent it is relevant to the question.
If the provided contexts contradict, say so. 
If there is context provided, but it is not relevant, say so.
If there is no context provided, say so.
Include an identification of the relevant context, as indicated by the markdown header, under which you find the answer.
Use three sentences maximum and keep the answer as concise as possible.
Always say "thanks for asking!" at the end of the answer.

{context}

Question: {question}

Helpful Answer:"""
prompt = PromptTemplate.from_template(template2)

# prompt = hub.pull("rlm/rag-prompt")
# print(prompt)

from langchain_core.runnables import RunnableParallel

rag_chain_from_docs = (
    RunnablePassthrough.assign(context=(lambda x: format_docs(x["context"])))
    | prompt
    | llm
    | StrOutputParser()
)

rag_chain_with_source = RunnableParallel(
    {"context": retriever, "question": RunnablePassthrough()}
).assign(answer=rag_chain_from_docs)

while True:
        print("\n\n--- Your question?")
        try:
            line = input()
        except EOFError:
            break
        response = rag_chain_with_source.invoke(line)
        print(response["answer"])

# rag_chain_with_source.invoke(line).pick("answer") ?
# rag_chain_with_source.pick("answer").invoke(line) ?