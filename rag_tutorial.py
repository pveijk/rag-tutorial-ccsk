# -*- coding: utf-8 -*-
"""Rag tutorial.ipynb

Adapted from https://python.langchain.com/v0.2/docs/tutorials/rag/

https://python.langchain.com/v0.2/docs/how_to/document_loader_pdf/
"""

import getpass
import os
from dotenv import load_dotenv

load_dotenv()

#os.environ["LANGCHAIN_TRACING_V2"] = "true"
#os.environ["LANGCHAIN_API_KEY"] = getpass.getpass()
# https://smith.langchain.com/ p@d1g.nl

#os.environ["OPENAI_API_KEY"] = getpass.getpass()
# user name peter@digitalinfrastructures.nl

from langchain_openai import ChatOpenAI

llm = ChatOpenAI(model="gpt-3.5-turbo-0125")

import bs4
from langchain import hub
from langchain_chroma import Chroma
from langchain_community.document_loaders import WebBaseLoader
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough
from langchain_openai import OpenAIEmbeddings
from langchain_text_splitters import RecursiveCharacterTextSplitter

#PDF loader
from langchain_community.document_loaders import PyPDFLoader

file_path = (
    "content/Domain 1 (FINAL)_ Cloud Computing Concepts & Architectures.pdf"
)
loader = PyPDFLoader(file_path)
pages = loader.load_and_split()

pages[0]

#print(pages[0].page_content[:300])
#print(pages[5].metadata)
type(pages[0])
docs = pages

# splitting
from langchain_text_splitters import RecursiveCharacterTextSplitter

text_splitter = RecursiveCharacterTextSplitter(
    chunk_size=1000, chunk_overlap=200, add_start_index=True
)
all_splits = text_splitter.split_documents(docs)

len(all_splits)
len(all_splits[0].page_content)
all_splits[13].page_content

#storing and retrieving
from langchain_chroma import Chroma
from langchain_openai import OpenAIEmbeddings

print("Retrieve documents, print 0")

vectorstore = Chroma.from_documents(documents=all_splits, embedding=OpenAIEmbeddings())

retriever = vectorstore.as_retriever(search_type="similarity", search_kwargs={"k": 6})

retrieved_docs = retriever.invoke("What is IaaS?")

len(retrieved_docs)

print(retrieved_docs[0].page_content)

#prompt engineering
from langchain import hub

prompt = hub.pull("rlm/rag-prompt")

example_messages = prompt.invoke(
    {"context": "filler context", "question": "filler question"}
).to_messages()

example_messages
print("the prompt content")
print(example_messages[0].content)

# building chain 1
from langchain_core.output_parsers import StrOutputParser
from langchain_core.runnables import RunnablePassthrough

def format_docs(docs):
    return "\n\n".join(doc.page_content for doc in docs)

rag_chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | prompt
    | llm
    | StrOutputParser()
)
print("the chunks")
for chunk in rag_chain.stream("What is Task Decomposition?"):
    print(chunk, end="", flush=True)

chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | prompt
)

#built in chains
from langchain.chains import create_retrieval_chain
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain_core.prompts import ChatPromptTemplate

system_prompt = (
    "You are an assistant for question-answering tasks. "
    "Use the following pieces of retrieved context to answer "
    "the question. If you don't know the answer, say that you "
    "don't know. Use three sentences maximum and keep the "
    "answer concise."
    "\n\n"
    "{context}"
)

prompt = ChatPromptTemplate.from_messages(
    [
        ("system", system_prompt),
        ("human", "{input}"),
    ]
)

question_answer_chain = create_stuff_documents_chain(llm, prompt)
rag_chain = create_retrieval_chain(retriever, question_answer_chain)

#response = rag_chain.invoke({"input": "What is Task Decomposition?"})
#print(response["answer"])

#for document in response["context"]:
#    print(document)
#    print()

#chain3. customizing the prompt
from langchain_core.prompts import PromptTemplate

template = """Use the following pieces of context to answer the question at the end.
If you don't know the answer, just say that you don't know, don't try to make up an answer.
Use three sentences maximum and keep the answer as concise as possible.
Always say "thanks for asking!" at the end of the answer.

{context}

Question: {question}

Helpful Answer:"""
custom_rag_prompt = PromptTemplate.from_template(template)

rag_chain = (
    {"context": retriever | format_docs, "question": RunnablePassthrough()}
    | custom_rag_prompt
    | llm
    | StrOutputParser()
)
print("\nfinal answer")
response = rag_chain.invoke("What is IaaS?")
print(response)