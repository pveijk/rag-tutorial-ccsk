# https://docs.google.com/document/d/1kZr95WfvPSckVFh_HJ32AbuWy0eSt2YvkZcM6NntD7k/edit?usp=sharing
# for feature request Git.
from langchain_googledrive.document_loaders import GoogleDriveLoader

doc_id='1kZr95WfvPSckVFh_HJ32AbuWy0eSt2YvkZcM6NntD7k'
folder_id='1VLBA1kJtaaP507-jrIjKnEmggn6Hy1tH'

loader = GoogleDriveLoader(
    folder_id=folder_id,
    recursive=False,
    num_results=2,  # Maximum number of file to load
    file_type=["application/vnd.google-apps.document"],
    suggestionsViewMode="PREVIEW_SUGGESTIONS_ACCEPTED",
    includeItemsFromAllDrives=True
)
docs = loader.load()
print(docs[0].page_content)
